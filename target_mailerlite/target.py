"""Mailerlite target class."""

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_mailerlite.sinks import MailerliteSink


class TargetMailerlite(Target):
    """Sample target for Mailerlite."""

    name = "target-mailerlite"
    config_jsonschema = th.PropertiesList(
        th.Property("api_key", th.StringType, required=True),
        th.Property(
            "group_ids",
            th.Property("group_ids", th.CustomType({"type": ["array", "string"]})),
        ),
    ).to_dict()
    default_sink_class = MailerliteSink


if __name__ == "__main__":
    TargetMailerlite.cli()
