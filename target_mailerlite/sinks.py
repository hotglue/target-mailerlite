"""Mailerlite target sink class, which handles writing streams."""


from datetime import datetime

import requests
from singer_sdk.sinks import RecordSink

from target_mailerlite.mapping import UnifiedMapping


class MailerliteSink(RecordSink):
    """Mailerlite target sink class."""

    total = 0
    base_url = "https://connect.mailerlite.com/api"

    @property
    def get_headers(self):
        headers = {}
        headers["Content-Type"] = "application/json"
        headers["Authorization"] = f"Bearer {self.config.get('api_key')}"
        return headers

    def process_contact(self, record):
        mapping = UnifiedMapping()
        url = f"{self.base_url}/subscribers"
        payload = mapping.prepare_payload(record, "contacts")

        if len(self.config.get("group_ids", [])) > 0:
            payload["groups"] = self.config.get("group_ids")

        res = requests.post(url, headers=self.get_headers, json=payload)
        self.post_message(res)

    def process_record(self, record: dict, context: dict) -> None:
        if self.stream_name.lower() in ["contacts", "customers"]:
            self.process_contact(record)

    def post_message(self, res):
        res.raise_for_status()
        self.total = self.total + 1
        print(f"Status: {res.status_code}, {self.total} records processed so far.")
