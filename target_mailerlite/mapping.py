import json
import os
from cgitb import lookup
from datetime import datetime

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


class UnifiedMapping:
    def __init__(self) -> None:
        pass

    def read_json_file(self, filename):
        # read file
        with open(os.path.join(__location__, f"{filename}"), "r") as filetoread:
            data = filetoread.read()

        # parse file
        content = json.loads(data)

        return content

    def map_lists(self, addresses, address_mapping, payload, type="billing_address"):
        address = {}
        for key in address_mapping.keys():
            address[address_mapping[key]] = addresses[key]
        payload[type] = address
        return payload

    def map_address(self, record, payload):
        if "addresses" in record:
            address = record.get("addresses")[0]
            if address.get("city"):
                payload["fields"].update({"city": address.get("city")})
            if address.get("state"):
                payload["fields"].update({"state": address.get("state")})
            if address.get("country"):
                payload["fields"].update({"country": address.get("country")})
        return payload

    def prepare_payload(self, record, endpoint="contact"):
        mapping = self.read_json_file(f"mapping.json")
        ignore = mapping["ignore"]
        mapping = mapping[endpoint]
        payload = {}
        payload_return = {}
        lookup_keys = mapping.keys()
        for lookup_key in lookup_keys:
            val = record.get(lookup_key, "")
            if val:
                payload[mapping[lookup_key]] = val

        if "name" in record and not "first_name" in record:
            first_name, last_name = record["name"].split(" ", 1)
            payload["fields"] = {"name": first_name, "last_name": last_name}
        elif "first_name" in record:
            payload["fields"] = {
                "name": record.get("first_name"),
                "last_name": record.get("last_name"),
            }
        # 2017-08-12T20:43:21.291Z
        if "subscribed_at" in record:
            payload["subscribed_at"] = datetime.strptime(
                payload["subscribed_at"], "%Y-%m-%dT%H:%M:%S.%fZ"
            ).strftime("%Y-%m-%d %H:%M:%S")
        else:
            payload["subscribed_at"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # Subscribe or unsubscribe based on status
        if "status" in payload:
            if payload["status"] == True:
                payload["status"] = "active"
            else:
                payload["status"] = "unsubscribed"

        # Subscribe or unsubscribe based on subscribe_status
        if "subscribe_status" in record:
            if record["subscribe_status"] == "unsubscribed":
                payload["status"] = "unsubscribed"

            else:
                payload["status"] = "active"

        if "status" not in payload:
            # By default subscribe the subscriber
            payload["status"] = "active"

        if "phone" in record:
            payload["fields"].update({"phone": record.get("phone")})

        if "phone_numbers" in record:
            if len(record.get("phone_numbers")) > 0:
                payload["fields"].update(
                    {"phone": record.get("phone_numbers")[0].get("number")}
                )
        payload = self.map_address(record, payload)
        # filter ignored keys
        for key in payload.keys():
            if key not in ignore:
                payload_return[key] = payload[key]
        return payload_return
